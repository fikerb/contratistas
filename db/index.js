'use strict';

const fs = require('fs');
const path = require('path');
const moment = require('moment-timezone');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const config = require('./config');
const db = {};

const sequelize = new Sequelize(config.database, config.username, config.password, {
  dialect: config.dialect,
  host: config.host,
  logging: false,
  port: config.port,
  define: {
    freezeTableName: true,
    timestamps: false,
  },
});

fs
  .readdirSync(path.join(__dirname, 'models'))
  .filter((file) => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach((file) => {
    const model = require(path.join(__dirname, 'models', file))(sequelize, Sequelize, fs, path, moment);

    db[model.name] = model;
  });

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
