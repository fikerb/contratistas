'use strict';

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query('create extension if not exists "uuid-ossp";');
  },
  down: () => {
    return new Promise((resolve) => {
      resolve();
    });
  },
};
