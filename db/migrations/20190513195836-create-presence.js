'use strict';

const model = require('../index').presence;

module.exports = {
  up: (queryInterface) => {
    delete model.structure.totemDateMomentObject;
    delete model.structure.totemDateMomentTZ;

    return queryInterface.createTable(model.name, model.structure, {
      comment: model.options.comment,
      uniqueKeys: model.uniqueKeys,
    });
  },

  down: (queryInterface) => {
    return queryInterface.dropTable(model.name);
  },
};

