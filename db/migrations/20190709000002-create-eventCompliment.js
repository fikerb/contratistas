'use strict';

const model = require('../index').eventCompliment;

module.exports = {
  up: (queryInterface) => {
    delete model.structure.complimentDateFormat;
    delete model.structure.complimentFile;
    delete model.structure.createdAtFormat;
    delete model.structure.validationDateFormat;
    delete model.structure.validationFile;

    return queryInterface.createTable(model.name, model.structure, {
      comment: model.options.comment,
      uniqueKeys: model.uniqueKeys,
    });
  },

  down: (queryInterface) => {
    return queryInterface.dropTable(model.name);
  },
};
