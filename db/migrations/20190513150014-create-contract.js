'use strict';

const model = require('../index').contract;

module.exports = {
  up: (queryInterface) => {
    delete model.structure.file;

    return queryInterface.createTable(model.name, model.structure, {
      comment: model.options.comment,
      uniqueKeys: model.uniqueKeys,
    });
  },

  down: (queryInterface) => {
    return queryInterface.dropTable(model.name);
  },
};
