'use strict';

const model = require('../index').event;

module.exports = {
  up: (queryInterface) => {
    delete model.structure.createdAtFormat;

    return queryInterface.createTable(model.name, model.structure, {
      comment: model.options.comment,
      uniqueKeys: model.uniqueKeys,
    });
  },

  down: (queryInterface) => {
    return queryInterface.dropTable(model.name);
  },
};
