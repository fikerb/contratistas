'use strict';

const dotenv = require('dotenv').config(); // eslint-disable-line no-unused-vars

const env = process.env.APP_ENV;
const dbName = process.env[`${ env }_DB_NAME`];
const dbTimezone = process.env[`${ env }_DB_TIMEZONE`];

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query(
      `ALTER DATABASE ${ dbName } SET timezone TO '${ dbTimezone }';`);
  },
  down: () => {
    return new Promise((resolve) => {
      resolve();
    });
  },
};
