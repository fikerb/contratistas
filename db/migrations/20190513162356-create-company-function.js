'use strict';

const model = require('../index').companyFunction;

module.exports = {
  up: (queryInterface) => {
    return queryInterface.createTable(model.name, model.structure, {
      comment: model.options.comment,
      uniqueKeys: model.uniqueKeys,
    });
  },

  down: (queryInterface) => {
    return queryInterface.dropTable(model.name);
  },
};
