'use strict';

const model = require('../index').incidentHistory;

module.exports = {
  up: (queryInterface) => {
    delete model.structure.file;
    delete model.structure.createdAtFormat;
    delete model.structure.plannedDateTimeFormat;

    return queryInterface.createTable(model.name, model.structure, {
      comment: model.options.comment,
      uniqueKeys: model.uniqueKeys,
    });
  },

  down: (queryInterface) => {
    return queryInterface.dropTable(model.name);
  },
};
