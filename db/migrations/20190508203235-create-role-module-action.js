'use strict';

const model = require('../index').roleModuleAction;

module.exports = {
  up: (queryInterface) => {
    return queryInterface.createTable(model.name, model.structure, {
      comment: model.options.comment,
    });
  },

  down: (queryInterface) => {
    return queryInterface.dropTable(model.name);
  },
};
