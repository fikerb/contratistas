'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: sequelize.fn('uuid_generate_v4'),
      primaryKey: true,
      type: DataTypes.UUID,
    },
    categoryId: {
      allowNull: false,
      comment: 'Categoria.',
      type: DataTypes.UUID,
      references: {
        model: 'type',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    name: {
      allowNull: false,
      comment: 'Nombre del tipo.',
      type: DataTypes.STRING,
    },
    resolutionTime: {
      allowNull: false,
      comment: 'Tiempo optimo de resolución del incidente',
      type: DataTypes.INTEGER,
    },
    firstEscalation: {
      allowNull: false,
      comment: 'Tiempo para el primer escalamiento',
      type: DataTypes.INTEGER,
    },
    secondEscalation: {
      allowNull: false,
      comment: 'Tiempo para el segundo escalamiento',
      type: DataTypes.INTEGER,
    },
    thirdEscalation: {
      allowNull: false,
      comment: 'Tiempo para el tercer escalamiento',
      type: DataTypes.INTEGER,
    },
    fourthEscalation: {
      allowNull: true,
      comment: 'Tiempo para el cuarto escalamiento',
      type: DataTypes.INTEGER,
    },
    enabled: {
      allowNull: false,
      comment: 'Indica si el registro está activo.',
      type: DataTypes.BOOLEAN,
    },
  };

  const model = sequelize.define('incidentType', structure, {
    comment: 'Tipos de incidentes.',
    modelName: 'incidentType',
  });

  model.structure = structure;

  model.associate = () => {};

  return model;
};
