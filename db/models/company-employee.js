'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      type: DataTypes.UUID,
    },
    companyId: {
      comment: 'Proveedor al cual pertenece.',
      allowNull: false,
      type: DataTypes.UUID,
      references: {
        model: 'company',
        key: 'id',
      },
      unique: 'uk_companyId_employeeId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    employeeId: {
      comment: 'Identificador único del empleado.',
      allowNull: false,
      type: DataTypes.UUID,
      references: {
        model: 'employee',
        key: 'id',
      },
      unique: 'uk_companyId_employeeId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    companyFunctionId: {
      comment: 'Identificador único de la función.',
      allowNull: true,
      type: DataTypes.UUID,
      references: {
        model: 'companyFunction',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    enabled: {
      allowNull: false,
      comment: 'Indica si el registro está activo.',
      type: DataTypes.BOOLEAN,
    },
  };

  const model = sequelize.define('companyEmployee', structure, {
    comment: 'Almacena empleados del proveedor.',
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsTo(models.company, {
      as: 'Company',
      foreignKey: 'companyId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.employee, {
      as: 'Employee',
      foreignKey: 'employeeId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.companyFunction, {
      as: 'CompanyFunction',
      foreignKey: 'companyFunctionId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.hasMany(models.presence, {
      as: 'Presences',
      foreignKey: 'companyEmployeeId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
