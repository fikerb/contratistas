'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: sequelize.fn('uuid_generate_v4'),
      primaryKey: true,
      type: DataTypes.UUID,
    },
    companyEmployeeId: {
      allowNull: true,
      comment: 'Empleado del proveedor al cual pertenece.',
      type: DataTypes.UUID,
    },
    coverageResultId: {
      allowNull: false,
      comment: 'Resultado del cálculo de cobertura al cual pertenece.',
      type: DataTypes.UUID,
    },
    exceededQuarter: {
      allowNull: false,
      comment: 'Cumplimiento excedido 25%.',
      type: DataTypes.BOOLEAN,
    },
    exceededHalf: {
      allowNull: false,
      comment: 'Cumplimiento excedido 50%.',
      type: DataTypes.BOOLEAN,
    },
    exception: {
      allowNull: false,
      comment: 'Indica si es excepción.',
      type: DataTypes.BOOLEAN,
    },
    incomplete: {
      allowNull: false,
      comment: 'Indica si una presencia incompleta.',
      type: DataTypes.BOOLEAN,
    },
    in: {
      allowNull: false,
      comment: 'Fecha y hora de entrada.',
      type: 'timestamp with time zone',
    },
    inId: {
      allowNull: false,
      comment: 'Presencia a la cual pertenece la entrada.',
      type: DataTypes.UUID,
    },
    out: {
      allowNull: true,
      comment: 'Fecha y hora de salida.',
      type: 'timestamp with time zone',
    },
    outId: {
      allowNull: true,
      comment: 'Presencia a la cual pertenece la salida.',
      type: DataTypes.UUID,
    },
    rut: {
      allowNull: false,
      comment: 'RUT del colaborador del proveedor.',
      type: DataTypes.STRING,
    },
  };

  const model = sequelize.define('coverageResultPresence', structure, {
    comment: 'Presencias del resultado del cálculo de cobertura.',
    modelName: 'coverageResultPresence',
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsTo(models.companyEmployee, {
      as: 'CompanyEmployee',
      foreignKey: 'companyEmployeeId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.presence, {
      as: 'PresenceIn',
      foreignKey: 'inId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.presence, {
      as: 'PresenceOut',
      foreignKey: 'outId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.hasOne(models.coverageResultPresencePlanPerformance, {
      as: 'planPerformance',
      foreignKey: 'coverageResultPresenceId',
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
