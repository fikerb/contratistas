'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      type: DataTypes.UUID,
    },
    contractId: {
      allowNull: false,
      comment: 'Contrato al cual pertenece.',
      type: DataTypes.UUID,
      references: {
        model: 'contract',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    escalationLevelId: {
      allowNull: false,
      comment: 'Nivel de escalamiento del contacto.',
      type: DataTypes.UUID,
      references: {
        model: 'type',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    firstName: {
      allowNull: false,
      comment: 'Primer nombre.',
      type: DataTypes.STRING(25),
    },
    fatherLastName: {
      allowNull: false,
      comment: 'Apellido paterno.',
      type: DataTypes.STRING(25),
    },
    email: {
      allowNull: false,
      comment: 'Correo electrónico.',
      type: DataTypes.STRING,
    },
    mobilePhoneNumber: {
      allowNull: true,
      type: DataTypes.INTEGER,
      comment: 'Telefono celular de contacto',
    },
  };

  const model = sequelize.define('contractContact', structure, {
    comment: 'Contactos de los contratos.',
    modelName: 'contractContact',
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsTo(models.type, {
      as: 'EscalationLevel',
      foreignKey: 'escalationLevelId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
