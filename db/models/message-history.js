'use strict';

module.exports = (sequelize, DataTypes, fs, path, moment) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: sequelize.fn('uuid_generate_v4'),
      primaryKey: true,
      type: DataTypes.UUID,
    },
    messageId: {
      allowNull: false,
      comment: 'mensaje.',
      type: DataTypes.UUID,
      references: {
        model: 'message',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    userId: {
      allowNull: false,
      comment: 'Usuario.',
      type: DataTypes.UUID,
      references: {
        model: 'user',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    message: {
      allowNull: false,
      comment: 'Mensaje.',
      type: DataTypes.TEXT,
    },
    createdAt: {
      allowNull: false,
      comment: 'Fecha de creación.',
      type: 'timestamp without time zone',
      defaultValue: DataTypes.NOW,
    },
    createdAtFormat: {
      type: new DataTypes.VIRTUAL(DataTypes.DATE, ['createdAt', ]),
      get() {
        return moment(this.get('createdAt')).format(process.env.LONG_DATE_FORMAT);
      },
    },
  };

  const model = sequelize.define('messageHistory', structure, {
    comment: 'Mensajeria.',
    modelName: 'messageHistory',
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsTo(models.message, {
      as: 'Message',
      foreignKey: 'messageId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.user, {
      as: 'User',
      foreignKey: 'userId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
