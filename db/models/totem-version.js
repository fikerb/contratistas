'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: sequelize.fn('uuid_generate_v4'),
      primaryKey: true,
      type: DataTypes.UUID,
    },
    version: {
      allowNull: false,
      comment: 'Numero de la versión.',
      type: DataTypes.STRING,
    },
    createdAt: {
      allowNull: false,
      comment: 'Fecha de creación del registro.',
      defaultValue: sequelize.fn('now'),
      type: 'timestamp without time zone',
    },
  };

  const model = sequelize.define('totemVersion', structure, {
    comment: 'Versión del totem.',
    modelName: 'totemVersion',
  });

  model.structure = structure;

  model.associate = () => {};

  return model;
};
