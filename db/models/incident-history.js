'use strict';

module.exports = (sequelize, DataTypes, fs, path, moment) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: sequelize.fn('uuid_generate_v4'),
      primaryKey: true,
      type: DataTypes.UUID,
    },
    incidentId: {
      allowNull: false,
      comment: 'Incidente.',
      type: DataTypes.UUID,
      references: {
        model: 'incident',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    statusId: {
      allowNull: false,
      comment: 'Estatus del incidente.',
      type: DataTypes.UUID,
      references: {
        model: 'type',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    userId: {
      allowNull: true,
      comment: 'Usuario.',
      type: DataTypes.UUID,
      references: {
        model: 'user',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    comment: {
      allowNull: true,
      comment: 'Comentario opcional.',
      type: DataTypes.TEXT,
    },
    plannedDateTime: {
      allowNull: true,
      comment: 'Fecha de programación.',
      type: 'timestamp without time zone',
    },
    plannedDateTimeFormat: {
      type: new DataTypes.VIRTUAL(DataTypes.DATE, ['plannedDateTime', ]),
      get() {
        return this.get('plannedDateTime') ? moment(this.get('plannedDateTime')).format(process.env.LONG_DATE_FORMAT)
          : null;
      },
    },
    createdAt: {
      allowNull: false,
      comment: 'Fecha de creación.',
      type: 'timestamp without time zone',
      defaultValue: DataTypes.NOW,
    },
    createdAtFormat: {
      type: new DataTypes.VIRTUAL(DataTypes.DATE, ['createdAt', ]),
      get() {
        return moment(this.get('createdAt')).format(process.env.LONG_DATE_FORMAT);
      },
    },
    file: {
      type: new DataTypes.VIRTUAL(DataTypes.TEXT, ['id', ]),
      get() {
        const fileExtension = process.env.IMAGE_EXTENSION;

        return fs.existsSync(`${ path.join(__dirname, '..', '..', process.env.UPLOAD_FOLDER,
          this.get('id')) }.${ fileExtension }`);
      },
    },
  };

  const model = sequelize.define('incidentHistory', structure, {
    comment: 'Historial del incidente.',
    modelName: 'incidentHistory',
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsTo(models.incident, {
      as: 'Incident',
      foreignKey: 'incidentId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.type, {
      as: 'Status',
      foreignKey: 'statusId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.user, {
      as: 'User',
      foreignKey: 'userId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
