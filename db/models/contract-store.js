'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      type: DataTypes.UUID,
    },
    contractId: {
      allowNull: false,
      type: DataTypes.UUID,
      references: {
        model: 'contract',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    storeId: {
      allowNull: false,
      type: DataTypes.UUID,
      references: {
        model: 'type',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
  };

  const model = sequelize.define('contractStore', structure, {
    comment: 'Almacena la relacion entre contratos y locales.',
    indexes: [
      {
        fields: ['contractId', 'storeId', ],
        name: 'custom_unique_constraint_name',
        unique: true,
      },
    ],
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsTo(models.contract, {
      as: 'Contract',
      foreignKey: 'contractId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    });

    model.belongsTo(models.type, {
      as: 'Store',
      foreignKey: 'storeId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    });
  };

  return model;
};
