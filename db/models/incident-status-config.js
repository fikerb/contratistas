'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: sequelize.fn('uuid_generate_v4'),
      primaryKey: true,
      type: DataTypes.UUID,
    },
    incidentStatusId: {
      allowNull: false,
      comment: 'Estatus asociado.',
      type: DataTypes.UUID,
      references: {
        model: 'type',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    roleIdSender: {
      allowNull: false,
      comment: 'Rol que crea el incidente.',
      type: DataTypes.UUID,
      references: {
        model: 'type',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    roleIdReceiver: {
      allowNull: false,
      comment: 'Rol que responde el incidente.',
      type: DataTypes.UUID,
      references: {
        model: 'type',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    nextStatusSender: {
      allowNull: true,
      comment: 'Configuración de respuesta del incidente para el propietario.',
      type: DataTypes.ARRAY(DataTypes.STRING),
    },
    nextStatusReceiver: {
      allowNull: true,
      comment: 'Configuración de respuesta del incidente para quien atiende.',
      type: DataTypes.ARRAY(DataTypes.STRING),
    },
    enabled: {
      allowNull: false,
      comment: 'Indica si el registro está activo.',
      type: DataTypes.BOOLEAN,
    },
  };

  const model = sequelize.define('incidentStatusConfig', structure, {
    comment: 'Configuracion de estatus de incidentes.',
    modelName: 'incidentStatusConfig',
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsTo(models.type, {
      as: 'IncidentStatus',
      foreignKey: 'incidentStatusId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.type, {
      as: 'RoleSender',
      foreignKey: 'roleIdSender',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.type, {
      as: 'RoleReceiver',
      foreignKey: 'roleIdReceiver',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
