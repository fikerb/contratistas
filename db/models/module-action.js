'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      type: DataTypes.UUID,

    },
    moduleId: {
      allowNull: false,
      comment: 'Módulo al cual pertenece.',
      type: DataTypes.UUID,
      references: {
        model: 'type',
        key: 'id',
      },
      unique: 'uk_moduleId_actionId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    actionId: {
      allowNull: false,
      comment: 'Acción a la cual pertenece.',
      type: DataTypes.UUID,
      references: {
        model: 'type',
        key: 'id',
      },
      unique: 'uk_moduleId_actionId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    code: {
      allowNull: false,
      comment: 'Identificador.',
      type: DataTypes.STRING(3),
      unique: true,
    },
  };

  const model = sequelize.define('moduleAction', structure, {
    comment: 'Relaciona un módulo con sus acciones.',
    sequelize,
    modelName: 'moduleAction',
  });

  model.structure = structure;

  model.associate = (models) => {
    model.hasMany(models.log, {
      as: 'Log',
      foreignKey: 'moduleActionId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
