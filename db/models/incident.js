'use strict';

module.exports = (sequelize, DataTypes, fs, path, moment) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: sequelize.fn('uuid_generate_v4'),
      primaryKey: true,
      type: DataTypes.UUID,
    },
    contractStoreId: {
      allowNull: false,
      comment: 'Contrato asociado al local.',
      type: DataTypes.UUID,
      references: {
        model: 'contractStore',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    incidentTypeId: {
      allowNull: false,
      comment: 'Tipo de incidente.',
      type: DataTypes.UUID,
      references: {
        model: 'incident',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    userId: {
      allowNull: false,
      comment: 'Identificador del usuario.',
      type: DataTypes.UUID,
      references: {
        model: 'user',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    code: {
      allowNull: false,
      comment: 'Descripción del incidente.',
      type: DataTypes.BIGINT,
      autoIncrement: true,
    },
    description: {
      allowNull: true,
      comment: 'Descripción del incidente.',
      type: DataTypes.TEXT,
    },
    automaticSolveOk: {
      allowNull: false,
      comment: 'Indica si fue validado automaticamente.',
      type: DataTypes.BOOLEAN,
    },
    createdAt: {
      allowNull: false,
      comment: 'Fecha de creación.',
      type: 'timestamp without time zone',
      defaultValue: DataTypes.NOW,
    },
    codeFilled: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING, ['code', ]),
      get() {
        return this.get('code') ? this.get('code').padStart(process.env.FILL_LENGTH, process.env.FILL_CHAR) : null;
      },
    },
    createdAtFormat: {
      type: new DataTypes.VIRTUAL(DataTypes.DATE, ['createdAt', ]),
      get() {
        return moment(this.get('createdAt')).format(process.env.LONG_DATE_FORMAT);
      },
    },
  };

  const model = sequelize.define('incident', structure, {
    comment: 'Incidentes.',
    scopes: {
      enabled: (action, incidents) => {
        return {
          where: {
            id: {
              [action]: incidents,
            },
          },
        };
      },
    },
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsTo(models.contractStore, {
      as: 'ContractStore',
      foreignKey: 'contractStoreId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.incidentType, {
      as: 'IncidentType',
      foreignKey: 'incidentTypeId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.user, {
      as: 'User',
      foreignKey: 'userId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.hasMany(models.incidentHistory, {
      as: 'IncidentHistory',
      foreignKey: 'incidentId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
