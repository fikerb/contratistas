'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: sequelize.fn('uuid_generate_v4'),
      primaryKey: true,
      type: DataTypes.UUID,
    },
    eventId: {
      allowNull: false,
      comment: 'Evento al que pertenece.',
      type: DataTypes.UUID,
      references: {
        model: 'event',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    frequencyId: {
      allowNull: false,
      comment: 'Tipo de frecuencia del evento.',
      type: DataTypes.UUID,
      references: {
        model: 'type',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    frequencyDetail: {
      allowNull: true,
      comment: 'Valor de la frecuencia.',
      type: 'int4[]',
    },
  };

  const model = sequelize.define('eventDetail', structure, {
    comment: 'Detalle de eventos.',
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsTo(models.type, {
      as: 'Frequency',
      foreignKey: 'frequencyId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.event, {
      as: 'Event',
      foreignKey: 'eventId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
