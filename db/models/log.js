'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: sequelize.fn('uuid_generate_v4'),
      primaryKey: true,
      type: DataTypes.UUID,
    },
    httpMethodId: {
      allowNull: false,
      comment: 'Método con el cual se realizó la petición.',
      type: DataTypes.UUID,
    },
    moduleActionId: {
      allowNull: true,
      comment: 'Módulo y acción al cual pertenece.',
      type: DataTypes.UUID,
    },
    userId: {
      allowNull: true,
      comment: 'Usuario al cual pertenece.',
      type: DataTypes.UUID,
    },
    fromIp: {
      allowNull: false,
      comment: 'Dirección IP desde la cual se realizó la petición.',
      type: DataTypes.INET,
    },
    userAgent: {
      allowNull: true,
      comment: 'Agente de usuario que realizó la petición (si aplica).',
      type: DataTypes.TEXT,
    },
    resource: {
      allowNull: false,
      comment: 'Endpoint solicitado.',
      type: DataTypes.TEXT,
    },
    statusCode: {
      allowNull: false,
      comment: 'Código de estado HTTP.',
      type: DataTypes.INTEGER,
    },
    request: {
      allowNull: true,
      comment: 'Request de la petición.',
      type: DataTypes.JSON,
    },
    response: {
      allowNull: true,
      comment: 'Response de la petición.',
      type: DataTypes.JSON,
    },
    createdAt: {
      allowNull: false,
      comment: 'Fecha de creación del registro.',
      defaultValue: DataTypes.NOW,
      type: DataTypes.DATE,
    },
  };

  const model = sequelize.define('log', structure, {
    comment: 'Almacena el registro de eventos de la aplicación.',
    sequelize,
    modelName: 'log',
  });

  model.structure = structure;

  model.associate = () => {};

  return model;
};
