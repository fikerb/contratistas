'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: sequelize.fn('uuid_generate_v4'),
      primaryKey: true,
      type: DataTypes.UUID,
    },
    calculatedPerformance: {
      allowNull: false,
      comment: 'Minutos cumplidos.',
      type: DataTypes.INTEGER,
    },
    calculatedPerformancePercent: {
      allowNull: false,
      comment: 'Minutos cumplidos (porcentual).',
      type: DataTypes.DECIMAL(10, 2),
    },
    calculatedPerformancePercentPresence: {
      allowNull: false,
      comment: 'Presencias cumplidas (porcentual).',
      type: DataTypes.DECIMAL(10, 2),
    },
    companyId: {
      allowNull: false,
      comment: 'Proveedor al que pertenece.',
      type: DataTypes.UUID,
      references: {
        model: 'company',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    contractId: {
      allowNull: false,
      comment: 'Contrato al que pertenece.',
      type: DataTypes.UUID,
      references: {
        model: 'contract',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    date: {
      allowNull: false,
      comment: 'Fecha calculada.',
      type: DataTypes.DATEONLY,
    },
    employeeQuantity: {
      allowNull: false,
      comment: 'Cantidad de empleados requeridos totales.',
      type: DataTypes.INTEGER,
    },
    employeeQuantityOriginal: {
      allowNull: false,
      comment: 'Cantidad de empleados requeridos del turno.',
      type: DataTypes.INTEGER,
    },
    endTime: {
      allowNull: false,
      comment: 'Hora de finalización del turno.',
      type: 'time without time zone',
    },
    exceededHalf: {
      allowNull: false,
      comment: 'Cumplimiento excedido 50%.',
      type: DataTypes.BOOLEAN,
    },
    exceededQuarter: {
      allowNull: false,
      comment: 'Cumplimiento excedido 25%.',
      type: DataTypes.BOOLEAN,
    },
    extraPerformance: {
      allowNull: false,
      comment: 'Cumplimiento adicional.',
      type: DataTypes.INTEGER,
    },
    initTime: {
      allowNull: false,
      comment: 'Hora de inicio del turno.',
      type: 'time without time zone',
    },
    performanceExpected: {
      allowNull: false,
      comment: 'Minutos planificados totales.',
      type: DataTypes.INTEGER,
    },
    performanceExpectedOriginal: {
      allowNull: false,
      comment: 'Minutos planificados del turno.',
      type: DataTypes.INTEGER,
    },
    planId: {
      allowNull: false,
      comment: 'Plaificación a la que pertenece.',
      type: DataTypes.UUID,
      references: {
        model: 'plan',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    storeId: {
      allowNull: false,
      comment: 'Local al cual pertenece.',
      type: DataTypes.UUID,
      references: {
        model: 'type',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    weekDays: {
      allowNull: false,
      comment: 'Dias de la semana del turno.',
      type: DataTypes.STRING,
    },
    weekDaysIndex: {
      allowNull: false,
      comment: 'Índice de los dias de la semana del turno.',
      type: DataTypes.ARRAY(DataTypes.INTEGER),
    },
  };

  const model = sequelize.define('coverageResult', structure, {
    comment: 'Resultado del cálculo de cobertura.',
    modelName: 'coverageResult',
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsTo(models.company, {
      as: 'Company',
      foreignKey: 'companyId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.contract, {
      as: 'Contract',
      foreignKey: 'contractId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.plan, {
      as: 'Plan',
      foreignKey: 'planId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.type, {
      as: 'Store',
      foreignKey: 'storeId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.hasMany(models.coverageResultPresence, {
      as: 'presences',
      foreignKey: 'coverageResultId',
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
