'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: sequelize.fn('uuid_generate_v4'),
      primaryKey: true,
      type: DataTypes.UUID,
    },
    coverageResultPresenceId: {
      allowNull: false,
      comment: 'Presencias del resultado del cálculo de cobertura a la cual pertenece.',
      type: DataTypes.UUID,
    },
    diffIn: {
      allowNull: false,
      comment: 'Minutos dentro del turno.',
      type: DataTypes.INTEGER,
    },
    diffOut: {
      allowNull: false,
      comment: 'Minutos fuera del turno.',
      type: DataTypes.INTEGER,
    },
  };

  const model = sequelize.define('coverageResultPresencePlanPerformance', structure, {
    comment: 'Cobertura de las presencias del resultado del cálculo de cobertura.',
    modelName: 'coverageResultPresencePlanPerformance',
  });

  model.structure = structure;

  model.associate = () => {};

  return model;
};
