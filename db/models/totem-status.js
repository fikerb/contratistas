'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      type: DataTypes.UUID,
    },
    totemId: {
      allowNull: false,
      type: DataTypes.UUID,
      references: {
        model: 'type',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    totemDate: {
      allowNull: false,
      type: DataTypes.DATE,
      get() {
        return moment.tz(
          this.getDataValue('totemDate'), process.env[`${ process.env.APP_ENV }_DB_TIMEZONE`]).format();
      },
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
  };

  const model = sequelize.define('totemStatus', structure, {
    comment: 'Registro del estado del totem.',
    modelName: 'totemStatus',
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsTo(models.type, {
      as: 'Totem',
      foreignKey: 'totemId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
