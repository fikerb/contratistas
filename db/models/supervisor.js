'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      type: DataTypes.UUID,
    },
    firstName: {
      allowNull: false,
      comment: 'Primer nombre.',
      type: DataTypes.STRING(25),
    },
    fatherLastName: {
      allowNull: false,
      comment: 'Apellido paterno.',
      type: DataTypes.STRING(25),
    },
    rut: {
      allowNull: false,
      comment: 'Identificador.',
      type: DataTypes.STRING,
      unique: true,
    },
    enabled: {
      allowNull: false,
      comment: 'Indica si el registro está activo.',
      type: DataTypes.BOOLEAN,
    },
  };

  const model = sequelize.define('supervisor', structure, {
    comment: 'Almacena los supervisores..',
    modelName: 'supervisor',
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsToMany(models.type, {
      as: 'SupervisorStore',
      foreignKey: 'supervisorId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      through: models.storeSupervisor,
    });
  };

  return model;
};
