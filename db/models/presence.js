'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: sequelize.UUIDV4,
      primaryKey: true,
      type: DataTypes.UUID,
    },
    accessTypeId: {
      allowNull: false,
      type: DataTypes.UUID,
      references: {
        model: 'type',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    companyEmployeeId: {
      allowNull: true,
      type: DataTypes.UUID,
      references: {
        model: 'companyEmployee',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    hash: {
      allowNull: true,
      type: DataTypes.STRING,
    },
    storeId: {
      allowNull: false,
      type: DataTypes.UUID,
      references: {
        model: 'type',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    totemId: {
      allowNull: true,
      type: DataTypes.UUID,
      references: {
        model: 'type',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    totemDate: {
      allowNull: false,
      type: DataTypes.DATE,
    },
    totemDateMomentObject: {
      type: new DataTypes.VIRTUAL(DataTypes.DATE, ['totemDate', ]),
      get() {
        return moment.tz(this.get('totemDate'), process.env[`${ process.env.APP_ENV }_DB_TIMEZONE`]);
      },
    },
    totemDateMomentTZ: {
      type: new DataTypes.VIRTUAL(DataTypes.DATE, ['totemDate', ]),
      get() {
        return moment.tz(this.get('totemDate'), process.env[`${ process.env.APP_ENV }_DB_TIMEZONE`]).format();
      },
    },
    validId: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
    },
    validFingerPrint: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
    },
    whiteList: {
      allowNull: false,
      comment: 'Pertenece a la lista blanca.',
      type: DataTypes.BOOLEAN,
    },
    bulkInsert: {
      allowNull: false,
      comment: 'Añadido por carga masiva.',
      type: DataTypes.BOOLEAN,
    },
    createdAt: {
      allowNull: false,
      defaultValue: DataTypes.NOW,
      type: DataTypes.DATE,
    },
  };

  const model = sequelize.define('presence', structure, {
    comment: 'Registra las presencias de los colaboradores.',
    modelName: 'presence',
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsTo(models.type, {
      as: 'AccessType',
      foreignKey: 'accessTypeId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.companyEmployee, {
      as: 'Employee',
      foreignKey: 'companyEmployeeId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.companyEmployee, {
      as: 'CompanyEmployee',
      foreignKey: 'companyEmployeeId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.type, {
      as: 'Store',
      foreignKey: 'storeId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.type, {
      as: 'Totem',
      foreignKey: 'totemId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.hasOne(models.presenceWithoutEmployee, {
      as: 'PresenceWithoutEmployee',
      foreignKey: 'presenceId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
