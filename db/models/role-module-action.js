'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      type: DataTypes.UUID,
    },
    roleId: {
      allowNull: false,
      comment: 'Rol al cual pertenece.',
      type: DataTypes.UUID,
      references: {
        model: 'type',
        key: 'id',
      },
      unique: 'fk_roleId_moduleActionId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    moduleActionId: {
      allowNull: false,
      comment: 'Módulo y acción a la cual pertenece.',
      type: DataTypes.UUID,
      references: {
        model: 'type',
        key: 'id',
      },
      unique: 'fk_roleId_moduleActionId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
  };

  const model = sequelize.define('roleModuleAction', structure, {
    comment: 'Relaciona un rol con un módulo y sus acciones.',
    modelName: 'roleModuleAction',
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsTo(models.moduleAction, {
      as: 'ModuleAction',
      foreignKey: 'moduleActionId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
