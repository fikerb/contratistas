'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      type: DataTypes.UUID,
    },
    rut: {
      allowNull: false,
      comment: 'RUT del proveedor.',
      unique: true,
      validate: {
        isAlphanumeric: true,
        len: [8, 9, ],
      },
      type: DataTypes.STRING(9),
    },
    name: {
      allowNull: false,
      comment: 'Nombre del proveedor.',
      type: DataTypes.STRING,
    },
    allowPresence: {
      allowNull: false,
      comment: 'Permite asignar presencias.',
      type: DataTypes.BOOLEAN,
    },
  };
  const model = sequelize.define('company', structure, {
    comment: 'Registro de proveedores.',
    modelName: 'company',
    scopes: {
      allowPresence: (allowPresence) => {
        return {
          where: {
            allowPresence,
          },
        };
      },
    },
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsToMany(models.employee, {
      as: 'Employees',
      foreignKey: 'companyId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      through: models.companyEmployee,
    });

    model.hasMany(models.contract, {
      as: 'Contracts',
      foreignKey: 'companyId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.hasMany(models.user, {
      as: 'Users',
      foreignKey: 'companyId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
