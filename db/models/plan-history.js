'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      type: DataTypes.UUID,
    },
    planId: {
      allowNull: false,
      comment: 'Planificación asociada.',
      type: DataTypes.UUID,
      references: {
        model: 'plan',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    turnId: {
      allowNull: false,
      comment: 'Turno asociado.',
      type: DataTypes.UUID,
      references: {
        model: 'turn',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    employeeQuantity: {
      allowNull: false,
      comment: 'Cantidad de empleados.',
      type: DataTypes.INTEGER,
    },
    initDateTime: {
      allowNull: false,
      comment: 'Fecha de inicio.',
      defaultValue: DataTypes.NOW,
      type: DataTypes.DATE,
    },
    endDateTime: {
      allowNull: true,
      comment: 'Fecha de finalización.',
      type: DataTypes.DATE,
    },
  };

  const model = sequelize.define('planHistory', structure, {
    comment: 'Historico de configuración de las planificaciones.',
    modelName: 'planHistory',
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsTo(models.turn, {
      as: 'Turn',
      foreignKey: 'turnId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.plan, {
      as: 'Plan',
      foreignKey: 'planId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
