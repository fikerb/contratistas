'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      type: DataTypes.UUID,
    },
    rut: {
      allowNull: false,
      comment: 'RUT del empleado.',
      type: DataTypes.STRING(9),
      unique: true,
    },
    firstName: {
      allowNull: false,
      comment: 'Primer nombre.',
      type: DataTypes.STRING(25),
    },
    middleName: {
      allowNull: true,
      comment: 'Segundo nombre.',
      type: DataTypes.STRING(25),
    },
    fatherLastName: {
      allowNull: false,
      comment: 'Apellido paterno.',
      type: DataTypes.STRING(25),
    },
    motherLastName: {
      allowNull: true,
      comment: 'Apellido materno.',
      type: DataTypes.STRING(25),
    },
  };

  const model = sequelize.define('employee', structure, {
    comment: 'Almacena el registro de los empleados.',
    modelName: 'employee',
  });

  model.structure = structure;

  model.associate = (models) => {
    model.hasOne(models.companyEmployee, {
      as: 'CompanyEmployee',
      foreignKey: 'employeeId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
