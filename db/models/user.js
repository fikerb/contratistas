'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: sequelize.fn('uuid_generate_v4'),
      primaryKey: true,
      type: DataTypes.UUID,
    },
    businessUnitId: {
      allowNull: true,
      comment: 'Unidad de negocios a la cual pertenece.',
      type: DataTypes.UUID,
    },
    companyId: {
      allowNull: true,
      comment: 'Proveedor al cual pertenece.',
      type: DataTypes.UUID,
      references: {
        model: 'company',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    roleId: {
      allowNull: false,
      comment: 'Rol al cual pertenece.',
      type: DataTypes.UUID,
      references: {
        model: 'type',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    storeId: {
      allowNull: true,
      comment: 'Local al cual pertenece.',
      type: DataTypes.UUID,
      references: {
        model: 'type',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    rut: {
      allowNull: false,
      comment: 'Identificador.',
      type: DataTypes.STRING(9),
      unique: true,
      validate: {
        len: [8, 9, ],
      },
    },
    firstName: {
      allowNull: false,
      comment: 'Primer nombre.',
      type: DataTypes.STRING(25),
    },
    middleName: {
      allowNull: true,
      comment: 'Segundo nombre.',
      type: DataTypes.STRING(25),
    },
    fatherLastName: {
      allowNull: false,
      comment: 'Apellido paterno.',
      type: DataTypes.STRING(25),
    },
    motherLastName: {
      allowNull: true,
      comment: 'Apellido materno.',
      type: DataTypes.STRING(25),
    },
    userName: {
      allowNull: false,
      comment: 'Identificador de inicio de sesión.',
      type: DataTypes.STRING(25),
      unique: true,
      validate: {
        isAlpha: true,
      },
    },
    email: {
      allowNull: false,
      comment: 'Correo electrónico.',
      type: DataTypes.STRING(250),
      unique: true,
      validate: {
        isEmail: true,
      },
    },
    password: {
      allowNull: false,
      comment: 'Contraseña encriptada.',
      type: DataTypes.TEXT,
    },
    enabled: {
      allowNull: false,
      comment: 'Indica si el registro está activo.',
      type: DataTypes.BOOLEAN,
    },
  };

  const model = sequelize.define('user', structure, {
    comment: 'Almacena los usuarios de la aplicación.',
    scopes: {
      enabled: {
        where: {
          enabled: true,
        },
      },
    },
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsTo(models.company, {
      as: 'Company',
      foreignKey: 'companyId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.hasMany(models.log, {
      as: 'Log',
      foreignKey: 'userId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.type, {
      as: 'BusinessUnit',
      foreignKey: 'businessUnitId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.type, {
      as: 'Store',
      foreignKey: 'storeId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.type, {
      as: 'Role',
      foreignKey: 'roleId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
