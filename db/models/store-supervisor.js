'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      type: DataTypes.UUID,
    },
    storeId: {
      allowNull: false,
      type: DataTypes.UUID,
      references: {
        model: 'type',
        key: 'id',
      },
      unique: 'uk_storeId_supervisorId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    supervisorId: {
      allowNull: false,
      type: DataTypes.UUID,
      references: {
        model: 'supervisor',
        key: 'id',
      },
      unique: 'uk_storeId_supervisorId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
  };

  const model = sequelize.define('storeSupervisor', structure, {
    comment: 'Asocia local y supervisor.',
    modelName: 'storeSupervisor',
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsTo(models.type, {
      as: 'Store',
      foreignKey: 'storeId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.supervisor, {
      as: 'Supervisors',
      foreignKey: 'supervisorId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.supervisor, {
      as: 'Supervisor',
      foreignKey: 'supervisorId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
