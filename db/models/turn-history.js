'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      type: DataTypes.UUID,
    },
    turnId: {
      allowNull: false,
      type: DataTypes.UUID,
      references: {
        model: 'turn',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    initTime: {
      allowNull: false,
      comment: 'Hora de entrada.',
      type: DataTypes.TIME,
    },
    endTime: {
      allowNull: false,
      comment: 'Hora de salida.',
      type: DataTypes.TIME,
    },
    initDateTime: {
      allowNull: false,
      comment: 'Fecha de inicio del turno.',
      defaultValue: DataTypes.NOW,
      type: DataTypes.DATE,
    },
    endDateTime: {
      allowNull: true,
      comment: 'Fecha de finalización del turno.',
      type: DataTypes.DATE,
    },
  };

  const model = sequelize.define('turnHistory', structure, {
    comment: 'Histórico de turnos.',
    sequelize,
    modelName: 'turnHistory',
  });

  model.structure = structure;

  model.associate = (models) => {
    model.hasMany(models.turnHistoryWeekDay, {
      as: 'TurnHistoryWeekDay',
      foreignKey: 'turnHistoryId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
