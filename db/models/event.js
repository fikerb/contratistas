'use strict';

module.exports = (sequelize, DataTypes, fs, path, moment) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: sequelize.fn('uuid_generate_v4'),
      primaryKey: true,
      type: DataTypes.UUID,
    },
    userId: {
      allowNull: false,
      comment: 'Identificador del usuario.',
      type: DataTypes.UUID,
      references: {
        model: 'user',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    contractStoreId: {
      allowNull: false,
      comment: 'Identificador del local.',
      type: DataTypes.UUID,
      references: {
        model: 'contractStore',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    enabled: {
      allowNull: false,
      comment: 'Estado del evendo (activo o inactivo).',
      type: DataTypes.BOOLEAN,
    },
    name: {
      allowNull: false,
      comment: 'Nombre del evento.',
      type: DataTypes.STRING(250),
    },
    plannedDate: {
      allowNull: true,
      comment: 'Fecha planificada.',
      type: DataTypes.DATEONLY,
    },
    onlySelected: {
      allowNull: true,
      comment: 'Solo dias seleccionados.',
      type: DataTypes.BOOLEAN,
    },
    createdAt: {
      allowNull: false,
      comment: 'Fecha de creación del registro.',
      defaultValue: DataTypes.NOW,
      type: 'timestamp without time zone',
    },
    createdAtFormat: {
      type: new DataTypes.VIRTUAL(DataTypes.DATE, ['createdAt', ]),
      get() {
        return moment(this.get('createdAt')).format(process.env.LONG_DATE_FORMAT);
      },
    },
  };

  const model = sequelize.define('event', structure, {
    comment: 'Registro de eventos.',
    modelName: 'event',
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsTo(models.user, {
      as: 'User',
      foreignKey: 'userId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.contractStore, {
      as: 'ContractStore',
      foreignKey: 'contractStoreId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.hasOne(models.eventDetail, {
      as: 'EventDetail',
      foreignKey: 'eventId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
