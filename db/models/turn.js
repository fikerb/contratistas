'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      type: DataTypes.UUID,
    },
    enabled: {
      allowNull: false,
      comment: 'Indica si el registro está activo.',
      type: DataTypes.BOOLEAN,
    },
  };

  const model = sequelize.define('turn', structure, {
    comment: 'Registro de turnos.',
    modelName: 'turn',
    scopes: {
      enabled: (enabled) => {
        return {
          where: {
            enabled,
          },
        };
      },
    },
  });

  model.structure = structure;

  model.associate = (models) => {
    model.hasOne(models.turnHistory, {
      as: 'TurnHistory',
      foreignKey: 'turnId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
