'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      type: DataTypes.UUID,
    },
    typeId: {
      allowNull: true,
      comment: 'Supertipo al cual pertenece.',
      type: DataTypes.UUID,
    },
    superId: {
      allowNull: true,
      comment: 'Registro al cual pertenece.',
      type: DataTypes.UUID,
    },
    code: {
      allowNull: true,
      comment: 'Identificador opcional.',
      type: DataTypes.STRING(25),
    },
    name: {
      allowNull: false,
      comment: 'Nombre del elemento.',
      type: DataTypes.STRING,
    },
    enabled: {
      allowNull: false,
      comment: 'Indica si el registro está activo.',
      type: DataTypes.BOOLEAN,
    },
  };

  const model = sequelize.define('type', structure, {
    comment: 'Almacena y relaciona los tipos y subtipos.',
    scopes: {
      enabled: (enabled) => {
        return {
          where: {
            enabled,
          },
        };
      },
      superId: (store) => {
        if (store) {
          return {
            where: {
              superId: {
                [DataTypes.Op.not]: null,
              },
            },
          };
        }

        return {
          where: {
            superId: null,
            enabled: true,
          },
        };
      },
    },
  });

  model.uniqueKeys = [
    {
      name: 'uk_typeId_code_name',
      fields: ['typeId', 'code', 'name', ],
      unique: true,
      type: 'UNIQUE',
    },
    {
      unique: true,
      fields: ['code', ],
    },
  ];

  model.structure = structure;

  model.associate = (models) => {
    model.hasOne(models.incidentStatusConfig, {
      as: 'IncidentStatusConfig',
      foreignKey: 'incidentStatusId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.hasOne(models.type, {
      as: 'StoreTotem',
      foreignKey: 'superId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.hasOne(models.type, {
      as: 'Prefix',
      foreignKey: 'superId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    // Zona de un local
    model.hasOne(models.zoneStore, {
      as: '_ZoneStore',
      foreignKey: 'storeId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.hasMany(models.contractStore, {
      as: 'StoreContracts',
      foreignKey: 'storeId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    });

    model.hasMany(models.contract, {
      as: 'CategoryContracts',
      foreignKey: 'categoryId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.hasMany(models.incidentType, {
      as: 'IncidentType',
      foreignKey: 'categoryId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.hasMany(models.log, {
      as: 'Log',
      foreignKey: 'httpMethodId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.hasMany(models.moduleAction, {
      as: 'ModuleActionAction',
      foreignKey: 'actionId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.hasMany(models.moduleAction, {
      as: 'ModuleActionModule',
      foreignKey: 'moduleId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.hasMany(models.roleModuleAction, {
      as: 'RoleModuleAction',
      foreignKey: 'roleId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.hasMany(models.type, {
      as: 'SubType',
      foreignKey: 'typeId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.hasMany(models.type, {
      as: 'Categories',
      foreignKey: 'superId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.hasMany(models.type, {
      as: 'BusinessUnitStores',
      foreignKey: 'superId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.hasMany(models.user, {
      as: 'Users',
      foreignKey: 'storeId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    // Locales asociados a la zona
    model.hasMany(models.zoneStore, {
      as: 'ZoneStores',
      foreignKey: 'zoneId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.type, {
      as: 'Type',
      foreignKey: 'typeId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.type, {
      as: 'SuperType',
      foreignKey: 'superId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.type, {
      as: 'WorkField',
      foreignKey: 'superId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.type, {
      as: 'TotemStore',
      foreignKey: 'superId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.type, {
      as: 'BusinessUnit',
      foreignKey: 'superId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsToMany(models.supervisor, {
      as: 'StoreSupervisor',
      foreignKey: 'storeId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      through: models.storeSupervisor,
    });

    model.belongsToMany(models.contract, {
      as: 'Stores',
      foreignKey: 'contractId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      through: models.contractStore,
    });

    model.belongsToMany(models.contract, {
      as: 'StoresContracts',
      foreignKey: 'storeId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      through: models.contractStore,
    });
  };

  return model;
};
