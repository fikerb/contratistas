'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      type: DataTypes.UUID,
    },
    contractStoreId: {
      allowNull: false,
      comment: 'Contrato y local asociado..',
      type: DataTypes.UUID,
      references: {
        model: 'contractStore',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    enabled: {
      allowNull: false,
      comment: 'Indica si el registro está activo.',
      type: DataTypes.BOOLEAN,
    },
  };

  const model = sequelize.define('plan', structure, {
    comment: 'Registro de planificaciones.',
    modelName: 'plan',
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsTo(models.contractStore, {
      as: 'ContractStore',
      foreignKey: 'contractStoreId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.hasOne(models.planHistory, {
      as: 'PlanHistory',
      foreignKey: 'planId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
