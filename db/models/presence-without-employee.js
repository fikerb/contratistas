'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      type: DataTypes.UUID,
    },
    companyId: {
      allowNull: false,
      type: DataTypes.UUID,
      references: {
        model: 'company',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    presenceId: {
      allowNull: false,
      type: DataTypes.UUID,
      references: {
        model: 'presence',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    storeSupervisorId: {
      allowNull: false,
      type: DataTypes.UUID,
      references: {
        model: 'storeSupervisor',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    rut: {
      allowNull: false,
      type: DataTypes.STRING,
    },
  };

  const model = sequelize.define('presenceWithoutEmployee', structure, {
    comment: 'Registra las excepciones de las presencias de los colaboradores.',
    modelName: 'presenceWithoutEmployee',
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsTo(models.presence, {
      as: 'Presence',
      foreignKey: 'presenceId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.company, {
      as: 'Company',
      foreignKey: 'companyId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.storeSupervisor, {
      as: 'StoreSupervisor',
      foreignKey: 'storeSupervisorId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
