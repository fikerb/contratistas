'use strict';

module.exports = (sequelize, DataTypes, fs, path) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      type: DataTypes.UUID,
    },
    categoryId: {
      allowNull: false,
      comment: 'Categoria del contrato.',
      type: DataTypes.UUID,
      references: {
        model: 'type',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    companyId: {
      allowNull: false,
      comment: 'Proveedor asociado al contrato.',
      type: DataTypes.UUID,
      references: {
        model: 'company',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    userId: {
      allowNull: false,
      comment: 'Usuario procurement administrador del contrato.',
      type: DataTypes.UUID,
      references: {
        model: 'user',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    name: {
      allowNull: false,
      comment: 'Nombre del contrato.',
      type: DataTypes.STRING,
    },
    initDate: {
      allowNull: false,
      comment: 'Fecha de inicio del contrato.',
      type: DataTypes.DATEONLY,
    },
    endDate: {
      comment: 'Fecha de finalización del contrato.',
      allowNull: false,
      type: DataTypes.DATEONLY,
    },
    file: {
      type: new DataTypes.VIRTUAL(DataTypes.TEXT, ['id', ]),
      get() {
        return fs.existsSync(`${ path.join(__dirname, '..', '..', process.env.UPLOAD_FOLDER,
          this.get('id')) }.${ process.env.DOCUMENT_EXTENSION }`);
      },
    },
    enabled: {
      allowNull: false,
      comment: 'Indica si el registro está activo.',
      type: DataTypes.BOOLEAN,
    },
  };

  const model = sequelize.define('contract', structure, {
    comment: 'Registro de contratos.',
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsTo(models.type, {
      as: 'Category',
      foreignKey: 'categoryId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.company, {
      as: 'Company',
      foreignKey: 'companyId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.user, {
      as: 'User',
      foreignKey: 'userId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    });

    model.hasMany(models.contractContact, {
      as: 'Contacts',
      foreignKey: 'contractId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsToMany(models.type, {
      as: 'Stores',
      foreignKey: 'contractId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      through: models.contractStore,
    });

    model.hasMany(models.contractStore, {
      as: 'StoresSave',
      foreignKey: 'contractId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    });
  };

  return model;
};
