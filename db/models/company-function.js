'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      type: DataTypes.UUID,
    },
    companyId: {
      comment: 'Proveedor al cual pertenece.',
      allowNull: false,
      type: DataTypes.UUID,
      references: {
        model: 'company',
        key: 'id',
      },
      unique: 'uk_companyId_name',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    code: {
      allowNull: false,
      comment: 'Identificador secundario.',
      type: DataTypes.INTEGER,
      autoIncrement: true,
    },
    name: {
      allowNull: false,
      comment: 'Nombre del elemento.',
      type: DataTypes.STRING,
      unique: 'uk_companyId_name',
    },
    enabled: {
      allowNull: false,
      comment: 'Indica si el registro está activo.',
      type: DataTypes.BOOLEAN,
    },
  };

  const model = sequelize.define('companyFunction', structure, {
    comment: 'Almacena empleados del proveedor.',
    modelName: 'companyFunction',
    scopes: {
      enabled: (enabled) => {
        return {
          where: {
            enabled,
          },
        };
      },
    },
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsTo(models.company, {
      as: 'Company',
      foreignKey: 'companyId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
