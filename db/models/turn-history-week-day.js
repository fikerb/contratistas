'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      type: DataTypes.UUID,
    },
    turnHistoryId: {
      allowNull: false,
      comment: 'Historico de turno al cual pertenece.',
      type: DataTypes.UUID,
      references: {
        model: 'turnHistory',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    weekDayId: {
      allowNull: false,
      comment: 'Dia asignado.',
      type: DataTypes.UUID,
      references: {
        model: 'type',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
  };

  const model = sequelize.define('turnHistoryWeekDay', structure, {
    comment: 'Dias del turno.',
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsTo(models.type, {
      as: 'WeekDay',
      foreignKey: 'weekDayId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
