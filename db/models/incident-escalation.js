'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: sequelize.fn('uuid_generate_v4'),
      primaryKey: true,
      type: DataTypes.UUID,
    },
    incidentId: {
      allowNull: false,
      comment: 'Incidente asociado.',
      type: DataTypes.UUID,
      references: {
        model: 'incident',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    escalationLevelId: {
      allowNull: true,
      comment: 'Nivel de escalamiento.',
      type: DataTypes.UUID,
      references: {
        model: 'type',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    createdAt: {
      allowNull: false,
      comment: 'Fecha de creación.',
      type: 'timestamp without time zone',
      defaultValue: DataTypes.NOW,
    },
  };

  const model = sequelize.define('incidentEscalation', structure, {
    comment: 'Configuracion de estatus de incidentes.',
    modelName: 'incidentEscalation',
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsTo(models.type, {
      as: 'EscalationLevel',
      foreignKey: 'escalationLevelId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });

    model.belongsTo(models.incident, {
      as: 'Incident',
      foreignKey: 'incidentId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
