'use strict';

module.exports = (sequelize, DataTypes) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: sequelize.fn('uuid_generate_v4'),
      primaryKey: true,
      type: DataTypes.UUID,
    },
    incidentId: {
      allowNull: true,
      comment: 'Incidente asociado.',
      type: DataTypes.UUID,
      references: {
        model: 'incident',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    enabled: {
      allowNull: false,
      comment: 'Indica si el registro está activo.',
      type: DataTypes.BOOLEAN,
    },
    createdAt: {
      allowNull: false,
      comment: 'Fecha de creación.',
      type: 'timestamp without time zone',
      defaultValue: DataTypes.NOW,
    },
  };

  const model = sequelize.define('message', structure, {
    comment: 'Mensajeria.',
    modelName: 'message',
  });

  model.structure = structure;

  model.associate = (models) => {
    model.belongsTo(models.incident, {
      as: 'Incident',
      foreignKey: 'incidentId',
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
      targetKey: 'id',
    });
  };

  return model;
};
