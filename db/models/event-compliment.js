'use strict';

module.exports = (sequelize, DataTypes, fs, path, moment) => {
  const structure = {
    id: {
      allowNull: false,
      comment: 'Clave primaria.',
      defaultValue: sequelize.fn('uuid_generate_v4'),
      primaryKey: true,
      type: DataTypes.UUID,
    },
    eventId: {
      allowNull: false,
      comment: 'Vínculo a tabla de contratos por local.',
      type: DataTypes.UUID,
      references: {
        model: 'event',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    userIdCompliment: {
      allowNull: false,
      comment: 'Usuarios de cumplimiento.',
      type: DataTypes.UUID,
      references: {
        model: 'user',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    userIdValidation: {
      allowNull: true,
      comment: 'Usuario de validación.',
      type: DataTypes.UUID,
      references: {
        model: 'user',
        key: 'id',
      },
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
    complimentDate: {
      allowNull: false,
      comment: 'Fecha de cumplimiento.',
      defaultValue: DataTypes.NOW,
      type: 'timestamp without time zone',
    },
    validationDate: {
      allowNull: true,
      comment: 'Fecha de validación.',
      type: 'timestamp without time zone',
    },
    complimentComment: {
      allowNull: true,
      comment: 'Comentario de cumplimiento..',
      type: DataTypes.STRING,
    },
    validationComment: {
      allowNull: true,
      comment: 'Comentario de validación.',
      type: DataTypes.STRING,
    },
    latLong: {
      allowNull: true,
      comment: 'Latitud y longitud (GPS).',
      type: DataTypes.ARRAY(DataTypes.DECIMAL),
    },
    automaticValidation: {
      allowNull: false,
      comment: 'Indica si fue validado automaticamente.',
      type: DataTypes.BOOLEAN,
    },
    createdAt: {
      allowNull: false,
      comment: 'Fecha de creación del registro.',
      defaultValue: DataTypes.NOW,
      type: 'timestamp without time zone',
    },
    createdAtFormat: {
      type: new DataTypes.VIRTUAL(DataTypes.DATE, ['createdAt', ]),
      get() {
        return moment(this.get('createdAt')).format(process.env.LONG_DATE_FORMAT);
      },
    },
    complimentDateFormat: {
      type: new DataTypes.VIRTUAL(DataTypes.DATE, ['complimentDate', ]),
      get() {
        return moment(this.get('complimentDate')).format(process.env.LONG_DATE_FORMAT);
      },
    },
    validationDateFormat: {
      type: new DataTypes.VIRTUAL(DataTypes.DATE, ['validationDate', ]),
      get() {
        return this.get('validationDate')
          ? moment(this.get('validationDate')).format(process.env.LONG_DATE_FORMAT)
          : null;
      },
    },
    complimentFile: {
      type: new DataTypes.VIRTUAL(DataTypes.TEXT, ['id', ]),
      get() {
        const fileExtension = process.env.IMAGE_EXTENSION;
        const fileName = `${ this.get('id') }-c`;

        return fs.existsSync(`${ path.join(__dirname, '..', '..', process.env.UPLOAD_FOLDER,
          fileName) }.${ fileExtension }`);
      },
    },
    validationFile: {
      type: new DataTypes.VIRTUAL(DataTypes.TEXT, ['id', ]),
      get() {
        const fileExtension = process.env.IMAGE_EXTENSION;
        const fileName = `${ this.get('id') }-v`;

        return fs.existsSync(`${ path.join(__dirname, '..', '..', process.env.UPLOAD_FOLDER,
          fileName) }.${ fileExtension }`);
      },
    },
  };

  const model = sequelize.define('eventCompliment', structure, {
    comment: 'Cumplimiento de los eventos',
    modelName: 'eventCompliment',
  });

  model.structure = structure;

  model.associate = () => {};

  return model;
};
