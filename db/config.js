'use strict';

const dotenv = require('dotenv').config(); // eslint-disable-line no-unused-vars

const env = process.env.APP_ENV;
const dbHost = process.env[`${ env }_DB_HOST`];
const dbName = process.env[`${ env }_DB_NAME`];
const dbPassword = process.env[`${ env }_DB_PASSWORD`];
const dbPort = parseInt(process.env[`${ env }_DB_PORT`], 10);
const dbUser = process.env[`${ env }_DB_USER`];

module.exports = {
  database: dbName,
  dialect: 'postgres',
  host: dbHost,
  migrationStorage: process.env.DB_MIGRATION_STORAGE,
  migrationStorageTableName: process.env.DB_MIGRATION_STORAGE_TABLE,
  password: dbPassword,
  port: dbPort,
  seederStorage: process.env.DB_SEEDER_STORAGE,
  seederStorageTableName: process.env.DB_SEEDER_STORAGE_TABLE,
  username: dbUser,
};
