'use strict';

const dotenv = require('dotenv').config(); // eslint-disable-line no-unused-vars
const http = require('http');

const env = process.env.APP_ENV;
const expressServer = require('./bin/express-server');
const httpHost = process.env[`${ env }_HTTP_HOST`];
const httpPort = parseInt(process.env[`${ env }_HTTP_PORT`], 10);

const server = http.createServer(expressServer);

server.timeout = parseInt(process.env.HTTP_TIMEOUT, 10);

server.listen(httpPort, httpHost, () => {
  process.stdout.write(`Servicio iniciado en el puerto ${ httpPort } (${ env.toLowerCase() })`);
});
