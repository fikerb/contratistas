'use strict';

// const _ = require('lodash');
// const fs = require('fs');
// const path = require('path');
// const nodemailer = require('nodemailer');
//
// const Company = require('../db/models/company').class;
// const Contract = require('../db/models/contract').class;
// const ErrorEnhanced = require('./error-enhanced');
// const User = require('../db/models/user').class;
// const Type = require('../db/models/type').class;
// const ZoneStore = require('../db/models/zone-store').class;

const helper = {
  getEmptyResponse,
  // getUserProfile,
  // isAlphaWithOneSpace,
  // isRutValid,
  // sendMail,
  // writeFileSync,
};

module.exports = helper;

function getEmptyResponse() {
  return {
    data: null,
    success: false,
  };
}

// function getUserProfile(req) {
//   const response = {
//     _businessUnits: null,
//     _categories: null,
//     _companies: null,
//     _contracts: null,
//     _stores: null,
//     _zones: null,
//     _user: null,
//   };
//   let typeProfile = null;
//
//   return User.findByPk(req.jwtData.userId, {
//     include: [
//       {
//         as: 'Role',
//         attributes: ['id', 'code', 'name', ],
//         model: Type,
//         required: true,
//       },
//     ],
//   })
//     .then((user) => {
//       if (!user) {
//         throw new ErrorEnhanced(404);
//       }
//
//       response._user = user.get({plain: true, });
//
//       if (user.storeId) {
//         typeProfile = 'STORE';
//
//         return Type.findByPk(user.storeId, {
//           include: [
//             {
//               as: 'Contracts',
//               attributes: ['id', 'companyId', ],
//               include: [
//                 {
//                   as: 'Category',
//                   attributes: ['id', ],
//                   model: Type,
//                   required: true,
//                 },
//               ],
//               model: Contract,
//               required: false,
//               through: {
//                 attributes: [],
//               },
//             },
//             {
//               as: 'BusinessUnit',
//               attributes: ['id', ],
//               model: Type,
//               required: true,
//             },
//             {
//               as: '_ZoneStore',
//               model: ZoneStore,
//               required: false,
//             },
//           ],
//           subQuery: false,
//         });
//       }
//
//       if (user.companyId) {
//         typeProfile = 'COMPANY';
//
//         return Company.findByPk(user.companyId, {
//           include: [
//             {
//               as: 'Contracts',
//               attributes: ['id', ],
//               include: [
//                 {
//                   as: 'Stores',
//                   attributes: ['id', ],
//                   include: [
//                     {
//                       as: 'BusinessUnit',
//                       attributes: ['id', ],
//                       model: Type,
//                       required: false,
//                     },
//                     {
//                       as: '_ZoneStore',
//                       model: ZoneStore,
//                       required: false,
//                     },
//                   ],
//                   model: Type,
//                   required: true,
//                   through: {
//                     attributes: [],
//                   },
//                 },
//                 {
//                   as: 'Category',
//                   attributes: ['id', ],
//                   model: Type,
//                   required: true,
//                 },
//               ],
//               model: Contract,
//               required: false,
//             },
//           ],
//           subQuery: false,
//         });
//       }
//
//       if (user.businessUnitId) {
//         typeProfile = 'BUSINESS_UNIT';
//
//         return Company.findAll({
//           attributes: ['id', ],
//           include: [
//             {
//               as: 'Contracts',
//               attributes: ['id', ],
//               model: Contract,
//               include: [
//                 {
//                   as: 'Stores',
//                   attributes: ['id', ],
//                   include: [
//                     {
//                       as: 'BusinessUnit',
//                       attributes: ['id', ],
//                       model: Type,
//                       required: true,
//                       where: {
//                         id: user.businessUnitId,
//                       },
//                     },
//                     {
//                       as: '_ZoneStore',
//                       model: ZoneStore,
//                       required: false,
//                     },
//                   ],
//                   model: Type,
//                   through: {
//                     attributes: [],
//                   },
//                   required: true,
//                 },
//                 {
//                   as: 'Category',
//                   attributes: ['id', ],
//                   model: Type,
//                   required: true,
//                 },
//               ],
//               required: true,
//             },
//           ],
//           subQuery: false,
//         });
//       }
//
//       return new Promise((resolve) => {
//         resolve();
//       });
//     })
//     .then((data) => {
//       if (typeProfile === 'STORE') {
//         response._businessUnits = [data.BusinessUnit.id, ];
//
//         response._categories = data.Contracts.map((contract) => {
//           return contract.Category.id;
//         });
//
//         response._companies = _.uniq(data.Contracts.map((contract) => {
//           return contract.companyId;
//         }));
//
//         response._contracts = data.Contracts.map((contract) => {
//           return contract.id;
//         });
//
//         response._stores = [data.id, ];
//
//         response._zones = data._ZoneStore ? [data._ZoneStore.zoneId, ] : [];
//       }
//
//       if (typeProfile === 'COMPANY') {
//         response._businessUnits = _.uniq(
//           _.flattenDeep(data.Contracts.map((contract) => {
//             return contract.Stores.map((store) => {
//               return store.BusinessUnit.id;
//             });
//           }))
//         );
//
//         response._categories = _.uniq(
//           _.flattenDeep(data.Contracts.map((contract) => {
//             return contract.Category.id;
//           }))
//         );
//
//         response._companies = [data.id, ];
//
//         response._contracts = data.Contracts.map((contract) => {
//           return contract.id;
//         });
//
//         response._stores = _.uniq(
//           _.flattenDeep(data.Contracts.map((contract) => {
//             return contract.Stores.map((store) => {
//               return store.id;
//             });
//           }))
//         );
//
//         response._zones = _.uniq(
//           _.flattenDeep(data.Contracts.map((contract) => {
//             return contract.Stores.map((store) => {
//               return store._ZoneStore ? store._ZoneStore.zoneId : [];
//             });
//           }))
//         );
//       }
//
//       if (typeProfile === 'BUSINESS_UNIT') {
//         response._businessUnits = _.uniq(
//           _.flattenDeep(data.map((el) => {
//             return el.Contracts.map((contract) => {
//               return contract.Stores.map((store) => {
//                 return store.BusinessUnit.id;
//               });
//             });
//           }))
//         );
//
//         response._categories = _.uniq(
//           _.flattenDeep(data.map((el) => {
//             return el.Contracts.map((contract) => {
//               return contract.Category.id;
//             });
//           }))
//         );
//
//         response._companies = data.map((el) => {
//           return el.id;
//         });
//
//         response._contracts = _.flattenDeep(data.map((el) => {
//           return el.Contracts.map((contract) => {
//             return contract.id;
//           });
//         }));
//
//         response._stores = _.uniq(
//           _.flattenDeep(data.map((el) => {
//             return el.Contracts.map((contract) => {
//               return contract.Stores.map((store) => {
//                 return store.id;
//               });
//             });
//           }))
//         );
//
//         response._zones = _.uniq(
//           _.flattenDeep(data.map((el) => {
//             return el.Contracts.map((contract) => {
//               return contract.Stores.map((store) => {
//                 return store._ZoneStore ? store._ZoneStore.zoneId : [];
//               });
//             });
//           }))
//         );
//       }
//
//       return response;
//     });
// }
//
// function isAlphaWithOneSpace(input) {
//   return new RegExp(/^[a-zA-Z]+\s?[a-zA-Z]+$/).test(input);
// }
//
// function isRutValid(input) {
//   const rutToValidateWithDv = input.toUpperCase()
//     .replace(/\./g, '')
//     .replace('-', '');
//
//   if (!new RegExp(/^0*(\d{1,3}(\d{3})*)?([\dkK])$/).test(rutToValidateWithDv)) {
//     return false;
//   }
//
//   const multipliers = [2, 3, 4, 5, 6, 7, ];
//   const rutToValidateDv = rutToValidateWithDv.substr(rutToValidateWithDv.length -1, 1);
//   const rutToValidateWithOutDv = rutToValidateWithDv.substr(0, rutToValidateWithDv.length - 1);
//   const rutToValidateWithOutDvReverse = rutToValidateWithOutDv.split('').reverse();
//
//   let calculatedDv = null;
//   let multipliersCount = 0;
//   let productSum = 0;
//   let tempDv = null;
//
//   for (let iRutReverse = 0; iRutReverse < rutToValidateWithOutDvReverse.length; iRutReverse += 1) {
//     productSum += rutToValidateWithOutDvReverse[iRutReverse] * multipliers[multipliersCount];
//     multipliersCount = iRutReverse === multipliers.length - 1 ? 0 : multipliersCount + 1;
//   }
//
//   tempDv = 11 - (productSum % 11);
//
//   if (tempDv === 10) {
//     calculatedDv = 'K';
//   } else if (tempDv === 11) {
//     calculatedDv = '0';
//   } else {
//     calculatedDv = tempDv;
//   }
//
//   return calculatedDv.toString() === rutToValidateDv.toString();
// }
//
// function sendMail(subject, content, recipients) {
//   const transport = nodemailer.createTransport({
//     pool: true,
//     service: process.env.MAIL_SERVICE,
//     auth: {
//       user: process.env.MAIL_USER,
//       pass: process.env.MAIL_PASSWORD,
//     },
//   });
//
//   transport.sendMail({
//     from: process.env.MAIL_USER,
//     to: recipients.join(','),
//     subject,
//     html: `${ content }`,
//   }, (err) => {
//     if (err) {
//       console.error(err);
//       return;
//     }
//     console.error('Correo enviado');
//   });
// }
//
// function writeFileSync(file, fileExtension, fileName) {
//   fs.writeFileSync(`${ path.join(__dirname, '..',
//     process.env.UPLOAD_FOLDER, fileName) }.${ fileExtension }`, file, 'base64');
// }
