'use strict';

const validator = require('validator');

const ErrorEnhanced = require('./error-enhanced');

const User = require('../db/models/user').class;
const Type = require('../db/models/type').class;
const RoleModuleAction = require('../db/models/role-module-action').class;
const ModuleAction = require('../db/models/module-action').class;

module.exports = authorize;

function authorize(req, moduleAction) {
  return new Promise((resolve, reject) => {
    if (typeof(req.jwtData.userId) !== 'string' || !validator.isUUID(req.jwtData.userId)) {
      reject(new ErrorEnhanced(400));
      return;
    }

    User.findByPk(req.jwtData.userId, {
      attributes: ['id', ],
      include: [{
        as: 'Role',
        model: Type,
        required: true,
        attributes: ['id', ],
        include: [{
          as: 'RoleModuleAction',
          model: RoleModuleAction,
          required: true,
          attributes: ['id', ],
          include: [{
            as: 'ModuleAction',
            model: ModuleAction,
            required: true,
            attributes: ['code', ],
          }, ],
        }, ],
      }, ],
    })
      .then((user) => {
        if (!user) {
          reject(new ErrorEnhanced(404));
        }

        const isAuthorized = user.get({ plain: true, }).Role.RoleModuleAction.map((item) => {
          return item.ModuleAction.code;
        }).indexOf(moduleAction) > -1;

        if (!isAuthorized) {
          reject(new ErrorEnhanced(403));
          return;
        }

        resolve();
      })
      .catch((userError) => {
        reject(new ErrorEnhanced(500, userError));
      });
  });
}