'use strict';

class ErrorEnhanced extends Error {
  constructor(statusCode, message = null) {
    let newMessage = null;

    if (message && message.name && message.name === 'SequelizeUniqueConstraintError'
      && (message.original && message.original.constraint)) {
      newMessage = `${ message.name }: ${ message.original.constraint }`;
    }

    super(newMessage ? newMessage : message);
    this.status = statusCode;

    switch (this.message.split(':')[0]) {
    case 'SequelizeForeignKeyConstraintError':
      this.message
        = 'ForeignKeyConstraintError|'
        + `${ this.message.split(':')[1].split('«')[1].split('»')[0] }|`
        + `${ this.message.split(':')[1].split('_')[1] }`;
      break;

    case 'SequelizeUniqueConstraintError':
      this.message = `UniqueConstraintError|${ this.message.split(':')[1].split('_')[1] }`;
      break;
    }
  }
}

module.exports = ErrorEnhanced;