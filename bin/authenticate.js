'use strict';

const validator = require('validator');

const ErrorEnhanced = require('./error-enhanced');
const jwt = require('./jwt');

const User = require('../db/models/user').class;

module.exports = authenticate;

function authenticate(req, res, next) {
  if (typeof(req.headers['x-access-token']) !== 'string') {
    return next(new ErrorEnhanced(400));
  }

  jwt.verify(req.headers['x-access-token'])
    .then((verifyData) => {
      if (!validator.isUUID(verifyData.payload)) {
        return next(new ErrorEnhanced(400));
      }

      if ((verifyData.exp * 1000) - new Date().getTime() <= 0) {
        return next(new ErrorEnhanced(401));
      }

      User.findByPk(verifyData.payload, {
        attributes: ['id', ],
      })
        .then((user) => {
          if (!user) {
            return next(new ErrorEnhanced(404));
          }

          req.jwtData = {
            userId: verifyData.payload,
          };

          return next();
        })
        .catch((userError) => {
          return next(new ErrorEnhanced(500, userError));
        });
    })
    .catch((error) => {
      console.error(error);
      return next(new ErrorEnhanced(error.name === 'TokenExpiredError' ? 401 : 403));
    });
}