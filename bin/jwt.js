'use strict';

const jsonwebtoken = require('jsonwebtoken');

const jwt = {
  sign,
  verify,
};

module.exports = jwt;

function sign(payload) {
  return jsonwebtoken.sign({ payload, }, process.env.JWT_KEY, { expiresIn: process.env.JWT_EXPIRATION, });
}

function verify(token) {
  return new Promise((resolve, reject) => {
    jsonwebtoken.verify(token, process.env.JWT_KEY, (err, decoded) => {
      if (err) {
        reject(err);
      } else {
        resolve(decoded);
      }
    });
  });
}