'use strict';

const fs = require('fs');
const moment = require('moment');
const path = require('path');

module.exports = (fileName, message) => {
  const currentDate = moment().format(process.env.LONG_DATE_FORMAT);
  const file = path.join(__dirname, '..', 'log', `${ fileName }.log`);

  if (!fs.existsSync(path.join(__dirname, '..', 'log'))) {
    fs.mkdirSync(path.join(__dirname, '..', 'log'));
  }

  fs.appendFile(file, `${ currentDate } | ${ message }\n`, (fsError) => {
    if (fsError) {
      console.error(fsError.message);
    }
  });
};