'use strict';

const DB = require('../db/index');

// const ModuleAction = require('../db/models/module-action').class;
// const Log = require('../db/models/log').class;
// const Type = require('../db/models/type').class;

const log = {
  save,
};

module.exports = log;

function save(moduleActionCode, req, res, statusCode = 200) {
  const payload = {
    fromIp: req.ip,
    userAgent: req.headers['user-agent'],
    resource: req.originalUrl,
    statusCode,
    request: ['GET', 'DELETE', ].indexOf(req.method) > -1 ? (req.params ? req.params : null) : req.body,
    response:
      (['GET', ].indexOf(req.method) > -1 && statusCode < 500)
      || (['DELETE', 'PUT', 'POST', ].indexOf(req.method) > -1 && req.originalUrl.includes('reports')) ? null : res,
  };

  if (req.jwtData) {
    payload.userId = req.jwtData.userId;
  }

  if (moduleActionCode) {
    DB.moduleAction.findOne({
      attributes: ['id', ],
      where: {
        code: moduleActionCode,
      },
    })
      .then((moduleAction) => {
        payload.moduleActionId = moduleAction.get({ plain: true, }).id;

        return DB.type.findOne({
          attributes: ['id', ],
          where: {
            name: req.method,
          },
        });
      })
      .then((httpMethod) => {
        payload.httpMethodId = httpMethod.get({ plain: true, }).id;

        return DB.log.create(payload);
      })
      .catch(() => {
        console.error('CORE/LOG: verifique el header x-access-token.');
      });

    return;
  }

  DB.type.findOne({
    attributes: ['id', ],
    where: {
      name: req.method,
    },
  })
    .then((httpMethod) => {
      payload.httpMethodId = httpMethod.get({ plain: true, }).id;
      return DB.log.create(payload);
    })
    .catch(() => {
      console.error('CORE/LOG: verifique el header x-access-token.');
    });
}
