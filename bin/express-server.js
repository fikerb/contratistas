'use strict';

const async = require('async');
const express = require('express');
const fs = require('fs');
const lodash = require('lodash');
const moment = require('moment-timezone');
const morgan = require('morgan');
const path = require('path');

// const authenticate = require('./authenticate');
const authorize = require('./authorize');
const ErrorEnhanced = require('./error-enhanced');
const helper = require('./helper');
const log = require('./log');
const DB = require('../db');

const { validationResult, } = require('express-validator/check');

const apiRoot = process.env.API_ROOT;
const expressServer = express();

const router = express.Router();

const docFolder = process.env.DOC_FOLDER;
const filesFolder = process.env.FILES_FOLDER;
const updateFolder = process.env.UPDATE_FOLDER;
const uploadFolder = process.env.UPLOAD_FOLDER;
const wwwFolder = process.env.WWW_FOLDER;

expressServer.use(morgan('dev'));
expressServer.use(express.static(path.join(__dirname, '..', wwwFolder)));
expressServer.use(`/${ docFolder }`, express.static(path.join(__dirname, '..', docFolder)));
expressServer.use(`/${ filesFolder }`, express.static(path.join(__dirname, '..', filesFolder)));
expressServer.use(`/${ uploadFolder }`, express.static(path.join(__dirname, '..', uploadFolder)));
expressServer.use(`/${ updateFolder }`, express.static(path.join(__dirname, '..', updateFolder)));
expressServer.use(setHeaders);

expressServer.use(apiRoot, express.json({ limit: process.env.HTTP_PAYLOAD_LIMIT, }));

expressServer.use(expressComplements);

getAllFiles(path.join(__dirname, '..', 'routes'))
  .map((file) => {
    return require(file);
  })
  .forEach((route) => {
    new route(router);
  });

expressServer.use(apiRoot, router);

expressServer.use(handleNotFound);
expressServer.use(handleError);

module.exports = expressServer;

function handleError(err, req, res, next) { // eslint-disable-line no-unused-vars
  const response = {
    message: err.message !== 'null' ? err.message : null,
    success: false,
  };

  if ((['GET', 'PUT', 'DELETE', ].indexOf(req.method) > -1) && ([401, 403, 404, ].indexOf(err.status) > -1)) {
    req.params = res.locals.params;
  }

  log.save(res.locals.moduleAction, req, response, err.status);
  res.status(err.status).json(response);
}

function handleNotFound(req, res) {
  if (req.url.includes('.exe')) {
    res.status(404).end();
    return;
  }

  res.redirect(`http://${ req.headers.host }`);
}

function setHeaders(req, res, next) {
  res.header('Access-Control-Allow-Origin', req.headers.origin ? req.headers.origin : '*');

  if (req.method === 'OPTIONS'){
    res.header('Access-Control-Allow-Headers',
      'Authorization, cache-control, Content-Type, If-Modified-Since, Set-Cookie, pragma, x-access-token');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');

    return res.sendStatus(200);
  }

  next();
}

function expressComplements(req, res, next) {
  req.async = async;
  req.authorize = authorize;
  req.db = DB;
  req.ErrorEnhanced = ErrorEnhanced;
  req.helper = helper;
  req.lodash = lodash;
  req.log = log;
  req.moment = moment;
  req.router = router;
  req.validationResult = validationResult;

  next();
}

function getAllFiles (dirPath, arrayOfFiles = []) {
  fs.readdirSync(dirPath).forEach((file) => {
    if (fs.statSync(dirPath + '/' + file).isDirectory()) {
      getAllFiles(path.join(dirPath, '/', file), arrayOfFiles);
    } else {
      arrayOfFiles.push(path.join(dirPath, '/', file));
    }
  });

  return arrayOfFiles;
}
