'use strict';
const baseUrl = {
  _get: '/:apiVersion/users/:data?',
  _post: '/:apiVersion/users',
  _put: '/:apiVersion/users/:uuid',
  passwordChange: '/:apiVersion/users/:uuid/password-change',
};
const controller = {
  v1: require('../controllers/v1/user'),
};
const validationRules = require('../requests/user');

module.exports = function (router) {
  router.get(baseUrl._get, (req, res, next) => {
    const moduleAction = 'UF';
    const response = req.helper.getEmptyResponse();

    res.locals.moduleAction = moduleAction;
    res.locals.params = req.params;

    if (typeof controller[req.params.apiVersion] === 'undefined') {
      return next(new req.ErrorEnhanced(404));
    }

    req.authorize(req, moduleAction)
      .then(() => {
        controller[req.params.apiVersion]._get(req, (getError, getData) => {
          if (getError) {
            return next(getError);
          }

          response.count = getData.count ? getData.count.length : undefined;
          response.data = getData;
          response.success = true;

          req.log.save(moduleAction, req, response);
          res.status(200).json(response);
        });
      })
      .catch((promiseError) => {
        return next(promiseError);
      });
  });

  router.post(baseUrl._post, validationRules.post, (req, res, next) => {
    const moduleAction = 'UC';
    const response = req.helper.getEmptyResponse();
    const validationErrors = req.validationResult(req);

    res.locals.moduleAction = moduleAction;

    if (typeof controller[req.params.apiVersion] === 'undefined') {
      return next(new req.ErrorEnhanced(404));
    }

    if (!validationErrors.isEmpty()) {
      return next(new req.ErrorEnhanced(400));
    }

    req.authorize(req, moduleAction)
      .then(() => {
        controller[req.params.apiVersion]._post(req, (postError, postData) => {
          if (postError) {
            return next(postError);
          }

          response.data = postData;
          response.success = true;

          req.log.save(moduleAction, req, response);
          res.status(201).json(response);
        });
      })
      .catch((promiseError) => {
        return next(promiseError);
      });
  });

  router.put(baseUrl._put, validationRules.put, (req, res, next) => {
    const moduleAction = 'UU';
    const response = req.helper.getEmptyResponse();
    const validationErrors = req.validationResult(req);

    res.locals.moduleAction = moduleAction;
    res.locals.params = req.params;

    if (typeof controller[req.params.apiVersion] === 'undefined') {
      return next(new req.ErrorEnhanced(404));
    }

    if (!req.params.uuid || !validationErrors.isEmpty()) {
      return next(new req.ErrorEnhanced(400));
    }

    req.authorize(req, moduleAction)
      .then(() => {
        controller[req.params.apiVersion]._put(req, (getError, getData) => {
          if (getError) {
            return next(getError);
          }

          response.data = getData;
          response.success = true;

          req.log.save(moduleAction, req, response);
          res.status(200).json(response);
        });
      })
      .catch((promiseError) => {
        return next(promiseError);
      });
  });

  router.put(baseUrl.passwordChange, validationRules.passwordChange, (req, res, next) => {
    const moduleAction = 'UU';
    const response = req.helper.getEmptyResponse();
    const validationErrors = req.validationResult(req);

    res.locals.moduleAction = moduleAction;
    res.locals.params = req.params;

    if (typeof controller[req.params.apiVersion] === 'undefined') {
      return next(new req.ErrorEnhanced(404));
    }

    if (!req.params.uuid || !validationErrors.isEmpty()) {
      return next(new req.ErrorEnhanced(400));
    }

    req.authorize(req, moduleAction)
      .then(() => {
        controller[req.params.apiVersion]._put(req, (putError, putData) => {
          if (putError) {
            return next(putError);
          }

          response.data = putData;
          response.success = true;

          req.log.save(moduleAction, req, response);
          res.status(200).json(response);
        });
      })
      .catch((promiseError) => {
        return next(promiseError);
      });
  });
};
